#!/bin/bash

set -e

CONNECTOR_ZIP="confluentinc-kafka-connect-s3-10.5.7.zip"

if [ ! -f "${CONNECTOR_ZIP}" ]; then
    wget https://d1i4a15mxbxib1.cloudfront.net/api/plugins/confluentinc/kafka-connect-s3/versions/10.5.7/confluentinc-kafka-connect-s3-10.5.7.zip
fi

mkdir -p plugins

unzip -o ${CONNECTOR_ZIP} -d plugins


