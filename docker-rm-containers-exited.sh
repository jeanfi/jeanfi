#!/bin/bash

set -e

CONTAINERS=`docker ps -a --filter "status=exited" -q`

if [ -z "$CONTAINERS" ]; then
        echo "No exited containers to remove"
else
        docker rm -f $CONTAINERS
fi        
