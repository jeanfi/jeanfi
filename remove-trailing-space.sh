#!/bin/bash

set -e

sed -i 's/[[:space:]]*$//' $1
