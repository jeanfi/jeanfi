#!/bin/bash

set -e

cat -s "$1" >/tmp/$$.tmp

cp /tmp/$$.tmp "$1"
