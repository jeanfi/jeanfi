#!/bin/bash


set -e

docker network create elk || echo "network already created"

docker run -d --network=elk --rm  --name elasticsearch  -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.10.1

docker run -d --rm  --name kibana --network=elk -p 5601:5601 kibana:7.10.1

