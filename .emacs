;; Emacs Configuration File

; green on black colors
(set-face-background 'default "black")
(set-face-foreground 'default "green")

; highlight selection
(transient-mark-mode 1)

; avoid welcome page on startup
(setq inhibit-startup-message t)

; display column info
(column-number-mode t)

; remove menu bar
(menu-bar-mode 0)

; make
(global-set-key [f9] 'compile)

; same than C-x b
(global-set-key [C-tab] 'switch-to-buffer)

; set default font
;(set-default-font "-misc-fixed-medium-r-normal--15-140-75-75-c-90-iso8859-1")
;(set-default-font "-unknown-DejaVu Sans Mono-normal-normal-normal-*-14-140-*-*-m-0-iso10646-1")
(set-default-font "-unknown-Ubuntu Mono-normal-normal-normal-*-*-*-*-*-m-0-iso10646-1")

; mouse scrolling
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse

; keyboard scrolling
(setq scroll-step 1)

(setq user-mail-address "changeit")
(setq user-full-name "Mr Changeit")

; space instead of tab
(setq-default indent-tabs-mode nil)

; indentation size for c-mode
(setq-default c-basic-offset 8)

(setq show-trailing-whitespace t)
(add-hook 'c-mode-hook
  (lambda()
    (setq show-trailing-whitespace t)))

(setq show-trailing-whitespace t)
(add-hook 'java-mode-hook
  (lambda()
    (setq show-trailing-whitespace t)))
