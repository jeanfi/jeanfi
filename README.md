# Simple but useful scripts

* [elk-start.sh](elk-start.sh): start a full ELK stack with docker.
* [gitignore.sh](gitignore.sh): add files into the .gitignore file.
* [java-project-cleanup.sh](java-project-cleanp.sh): cleanup the files of a
  Java project (remove trailing white-spaces and consecutive empty lines).
* [java-version](java-version): print only the version of the Java CLI.
* [mdl-docker](mdl-docker): run markdown linter in a Docker container.
* [mvn-upgrade-properties.sh](mvn-upgrade-properties.sh): update properties
  to the latest versions of the artifacts.
* [remove-empty-lines.sh](remove-empty-lines.sh): remove consecutive empty lines.
* [remove-trailing-space.sh](remove-trailing-space.sh): remove trailing white spaces.

## Related to Docker

* [docker-rm-containers-exited](docker-rm-containers-exited.sh): remove
  exited docker containers.
* [docker-rm-images-untagged.sh](docker-rm-images-untagged.sh): remove
  untagged docker images.
* [kubectl-set-default-namespace.sh](kubectl-set-default-namespace.sh):
  set default kubectl namespace.

## Related to Microk8s

### Kafka

Ref: [Strimzi](https://strimzi.io/quickstarts/)

```sh
kubectl create namespace kafka
kubectl create -f 'https://strimzi.io/install/latest?namespace=kafka'
-n kafka
kubectl apply -f https://strimzi.io/examples/latest/kafka/
kafka-persistent-single.yaml -n kafka
```

Wait kafka to be up and running:

```
kubectl wait kafka/my-cluster --for=condition=Ready --timeout=300s -n kafka
```

Run a console producer:

```
kubectl -n kafka run kafka-producer -ti --image=quay.io/strimzi/
kafka:0.29.0-kafka-3.2.0 --rm=true --restart=Never -- bin/
kafka-console-producer.sh --bootstrap-server
my-cluster-kafka-bootstrap:9092 --topic my-topic
```

Run a console consumer:

```
kubectl -n kafka run kafka-consumer -ti --image=quay.io/strimzi/
kafka:0.29.0-kafka-3.2.0 --rm=true --restart=Never -- bin/
kafka-console-consumer.sh --bootstrap-server
my-cluster-kafka-bootstrap:9092 --topic my-topic --from-beginning
```