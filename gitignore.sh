#!/bin/bash

for i in "$@"
do
        DIR=`dirname $i`
        NAME=`basename $i`
        IGNOREFILE="$DIR/.gitignore"

        echo "Updated $IGNOREFILE with $NAME"

        echo "$NAME" >> "$IGNOREFILE"
done
