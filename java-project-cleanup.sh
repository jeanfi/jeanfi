#!/bin/bash

set -e

function usage() {
        echo "Usage: $0 [DIR]"
}

if [ -z "$1" ]; then
        usage
        exit 1
else
        DIR="$1"
fi

RPATH=`realpath $0`
SDIR=`dirname $RPATH`

FIND_SELECT=" -type f \( -name \*java -or -name pom.xml \) "

find "$DIR" -type f \( -name \*java -or -name pom.xml \) -exec "$SDIR/remove-trailing-space.sh" {} \;
find "$DIR" -type f \( -name \*java -or -name pom.xml \) -exec "$SDIR/remove-empty-lines.sh" {} \;
